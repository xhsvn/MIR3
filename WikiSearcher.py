import json
from os import listdir
import random
from collections import defaultdict
from math import sqrt, inf, log2

import operator
import random
from collections import defaultdict
from math import sqrt, inf, log2
from document import Document
from elasticsearch import Elasticsearch
from elasticsearch_dsl import Index, Search, Q
from elasticsearch_dsl.connections import connections
from tqdm import tqdm
from hazm import word_tokenize
from pagerank import PageRank


class Elastic:
    fields = ['title', 'introduction', 'contents']
    dir = 'jsons/'
    host = 'http://127.0.0.1:9200/'

    stopwords = ['از', 'تا', 'که', 'در', 'با', 'و', 'به', 'را', 'پس', ' یا', 'است']
    connections.create_connection(hosts=[host], port=9200, )

    def __init__(self):
        self.index_name = 'wiki'

    client = Elasticsearch()

    def create_index(self, name='wiki'):
        self.index_name = name
        index = Index(name)
        index.delete(ignore=404)
        index.settings(number_of_shards=5, number_of_replicas=1)
        index.doc_type(Document)
        index.create()
        self.add_docs()

    def import_to_elastic(self, data):
        doc = Document(meta={'id': data['pk']})
        doc.save(data=data)

    def add_docs(self):
        files = listdir(self.dir)
        pbar = tqdm(total=len(files))
        for f in files:
            with open(self.dir + f) as json_file:
                self.import_to_elastic(data=json.load(json_file))
                pbar.update(1)
        pbar.close()

    def rm_index(self, name=None):

        rm_name = name if name else self.index_name
        self.client.indices.delete(index=rm_name, ignore=[400, 404])

    def read_all_docs(self):
        query = {"query": {"match_all": {}}}
        response = self.client.search(index=self.index_name, body=query)
        size = response['hits']['total']
        response = self.client.search(index=self.index_name, body=query, size=size)
        return [h['_source'] for h in response['hits']['hits']]

    def get_clussters(self):
        alldoc = self.read_all_docs()
        return {d['cluster_title']: d['cluster_id'] for d in alldoc}

    def make_vectors(self):
        alldoc = self.read_all_docs()
        self.lenalldoc = len(alldoc)
        print('len_alldocs:', self.lenalldoc)
        self.terms = list(set(t for d in alldoc for t in d['contents'].split()))
        
        self.term_to_index = {}
        for i in range(len(self.terms)):
            self.term_to_index[self.terms[i]] = i
        self.title_to_pk = {}
        self.pk_to_vector = {}


        self.closters_to_id = {}
        self.closter_to_name = {}
        self.term_to_number = {}
        print('make vectors:')
        pbar = tqdm(total=len(alldoc))
        for document in alldoc:
            pk = document['pk']
            text =  document['contents']
            # v = self.vector_space_doc(pk)
            v = self.vector_space_doc_text(text)
            pbar.update(1)
            # print(pk)
            self.pk_to_vector[pk] = v
        pbar.close()
        print('end')

    def vector_space_doc_text(self, text):
        # doc_terms = word_tokenize(yechizi)
 
        tokens = text.split()
        vector = [0] * (len(self.terms) + 1)
        for word in tokens:
            # index = self.terms.index(word)
            # pbar.update(1)
            vector[self.term_to_index[word] + 1] += 1
        # pbar.close()
        return vector



    def vector_space_doc(self, pk):
        # doc_terms = word_tokenize(yechizi)

        dtf = self.client.termvectors(index=self.index_name, doc_type="document", id=pk, fields=['contents'])
        vector = [0] * (len(self.terms) + 1)
        dtf = dtf['term_vectors']
        if 'contents' not in dtf: return vector

        dtf = dtf['contents']['terms']


        for word in dtf:
                vector[self.term_to_index[word] + 1] = dtf[word]['term_freq']
        return vector

    def k_menas(self, k):
        print(k)
        cluster_start = random.sample(range(1, self.lenalldoc+1), k)
        # print("doc started "  +str(cluster_start))
        # cluster_center = {}
        cluster_group = defaultdict(list)
        for i in range(1, k + 1):
            cluster_group[i].append(cluster_start[i - 1])
        # print("now is my center    " + str(cluster_group))
        cost = 0
        for j in range(1, 6):
            cluster_center = self.calc_center(cluster_group)
            print('j', j)
            for key in self.pk_to_vector:
                dist = +inf
                id = 0
                for c in cluster_center:
                    distan = self.distance(self.pk_to_vector[key], cluster_center[c])
                    if distan < dist:
                        dist = distan
                        id = c
                for gp in cluster_group:
                    if key in cluster_group[gp]:
                        cluster_group[gp].remove(key)
                cluster_group[id].append(key)
                cost += dist
        return cluster_group, cost

    def distance(self, v1, v2):
        # print(len(v1), len(v2))
        s = 0
        for i in range(1, len(v1)):
            s = s + ((v1[i] - v2[i]) * (v1[i] - v2[i]))
        s = sqrt(s)
        return s

    def calc_center(self, cluster_group):
        cluster_center = {}
        for cluster in cluster_group:
            number_doc = len(cluster_group[cluster])
            vector = [0] * (len(self.terms)+1)
            for doc in cluster_group[cluster]:
                for i in range(0, (len(self.terms)+1)):
                    # print(i)
                    # print(doc)

                    vector[i] += self.pk_to_vector[doc][i]
            for i in range(0, (len(self.terms)+1)):
                vector[i] = vector[i] / number_doc
            cluster_center[cluster] = vector
        return cluster_center

    def kMenas(self, L):
        t = True
        i = 1
        save_cost = +inf
        z_save = {}
        if (L == -1):
            while t and i <= self.lenalldoc:
                z = self.k_menas(i)
                cost = z[1]
                print("K means cost for k =", str(i), "is ", str(cost))
                i += 1
                if cost / save_cost > 0.95:
                    print("choosen k is " + str(i - 2))
                    t = False
                    return z_save
                if i == self.lenalldoc:
                    print("choosen k is " + str(i))
                    return z
                z_save = z
                save_cost = cost


        else:
            while (t and i < L + 1):

                z = self.k_menas(i)
                cost = z[1]
                print("K means cost for k =", str(i), "is ", str(cost))
                if i == L:
                    print("choosen k is " + str(i))
                    return z
                i += 1
                if cost / save_cost > 0.95:
                    print("choosen k is " + str(i - 2))
                    t = False
                    return z_save
                z_save = z
                save_cost = cost


    def label_cluster(self, L):
        print('start clustterings')
        closters_to_id = self.kMenas(L)[0]
        print('end of clusstering')
        for c in closters_to_id:

            term_score = {}
            for i in range(0, (len(self.terms)+1)):
                N01 = 1
                N10 = 1
                N11 = 1
                N00 = 1
                score = 0
                for pk in self.pk_to_vector:
                    if (pk in closters_to_id[c] and self.pk_to_vector[pk][i] > 0):
                        N11 += 1
                    elif (pk in closters_to_id[c] and not (self.pk_to_vector[pk][i] > 0)):
                        N01 += 1
                    elif (not (pk in closters_to_id[c]) and (not (self.pk_to_vector[pk][i] > 0))):
                        N00 += 1
                    else:
                        N10 += 1
                N = N11 + N10 + N01 + N00
                N1 = N10 + N11
                N0 = N11 + N01
                score = (N11 / N) * log2((N * N11) / (N1 * N0)) + (N01 / N) * log2(
                    (N * N01) / ((N11 + N01) * (N01 + N00))) \
                        + (N10 / N) * log2((N * N10) / (N1 * (N00 + N10))) + (N00 / N) * log2(
                    (N00 * N) / ((N01 + N00) * (N10 + N00)))
                term_score[i] = score
            sorted_score = sorted(term_score.items(), key=operator.itemgetter(1))
            count = 1
            k = 0
            name = ""
            while (count < 6):
                q = sorted_score[k][0]
                if not (self.terms[sorted_score[k][0]] in self.stopwords):
                    name = name + " " + self.terms[q]
                    count += 1
                k += 1
            self.closter_to_name[c] = name
        return closters_to_id
        # print(closter_to_name)


    def third(self, limit):
        self.make_vectors()
        # print('ffdfkjdkfjkjfskdjfkd')
        closters_to_id = self.label_cluster(limit)
        # for t, d in self.closter_to_name.items():
        #     print(t)
        print('add clusters to server')
        pbar = tqdm(total=self.lenalldoc)
        for clussted_id, pks in closters_to_id.items():
            for pk in pks:
                # print(pk, self.closter_to_name[clussted_id])
                pbar.update(1)
                self.client.update(index=self.index_name, doc_type='document', id=pk, body={'doc': {'cluster_id': clussted_id, 'cluster_title': self.closter_to_name[clussted_id]}})
        pbar.close()
        # print(closters_to_id)
        # print(self.closter_to_name)

    def page_rank(self, alpha):
        client = Elasticsearch()
        docs = self.read_all_docs()
        url_to_pk = {doc['url']: doc['pk'] for doc in docs}
        n = len(docs)
        matrix = [[0]*n for i in range(n)]

        for doc in docs:
            for link in doc['links']:
                if link != 'wth' and link in url_to_pk:
                    matrix[doc['pk']-1][url_to_pk[link]-1] = 1
        ranks = PageRank(matrix, alpha).get_rank()
        pbar = tqdm(total=n)
        for i in range(n):
            self.client.update(index=self.index_name, doc_type='document', id=i+1, body={'doc': {'pagerank': ranks[i]}})
            pbar.update(1)
        pbar.close()

    def get_doc(self, pk):
        client = Elasticsearch()
        s = Search(using=client, index=self.index_name)
        s.query(Q('bool', filter=[Q('terms', pk=[pk])]))
        r = s.execute()
        print(r[0].url)
#
    def search(self, weights=[1, 1, 1], cluster=-1, pagerank=False, query='سعدی'):
        boosted_fields = [self.fields[i] + '^' + str(weights[i]) for i in range(len(weights))]
        client = Elasticsearch()

        cluster_filter = Q('bool', filter=[Q('terms', cluster_id=[cluster])])
        search_query = Q("multi_match", query=query, fields=boosted_fields)
        s = Search(using=client, index=self.index_name)

        s = s.query(search_query) if cluster is -1 else s.query(cluster_filter).query(search_query)

        if pagerank:
            s = s.sort({'pagerank': {"order": "desc"}})

        response = s.execute()
        for hit in response:
            print(hit.pk, hit.title)


