from WikiSearcher import Elastic
from WIKISEARCH.WIKISEARCH.spiders.wikispider import run
e = Elastic()


def main():
    commands = {'1': first, '2': second, '3': third, '4': forth, '5': fifth, 'q': exit}
    while True:
        command = input('1st part[1], 2nd part[2], 3rd part[3], 4th part[4], 5th part[5], quit[q]: ')
        commands[command]() if command in commands else None

############################################## first part ##################################################
def first():
    number = int(input('enter number of start_urls: '))
    start_urls = []
    for i in range(number):
        start_urls.append(input())
    out_degree = int(input('out_degree: '))
    n = int(input('N:'))

    run(n, out_degree, start_urls)


############################################## second part ##################################################
def second():
    commands = {'1': build_index, '2': remove_doc}
    while True:
        command = input('build index[1], remove index[2], back[b]: ')
        if command == 'b': return
        commands[command]() if command in commands else None


def build_index():
    doc_name = input('index_name:')
    if len(doc_name):
        e.create_index(doc_name)
    else:
        e.create_index()
    print('access url:', e.host + e.index_name + '/_search?pretty=t')


def remove_doc():
    index_name = input('enter name of the index or none to remove just created index : ')
    e.rm_index(index_name if index_name else None)


def show_postinglist():
    token = input('enter a word: ')
    token = g.parser(token)[0]
    for doc, content in g.positional_index[token].items():
        print(str(doc)+'.persian_poem:', content)


def show_bigram():
    token = input('enter a word: ')
    print(*(g.bigram_index[token]) if token in g.bigram_index else 'not found', sep='\n')

############################################## third part ##################################################
def third():

    search_type = input('inter l(-1 in case of unlimited) , back[b]: ')
    if search_type == 'b': return
    l = int(search_type)
    e.third(l)
    print('access url:', e.host + e.index_name + '/_search?pretty=t')

############################################## forth part ##################################################

def forth():
    command = input('enter alpha or back[b]: ')
    if command == 'b': return
    alpha = float(command)
    e.page_rank(alpha)
    print('access url:', e.host + e.index_name + '/_search?pretty=t')


############################################## fifth part ##################################################
def fifth():
    while True:
        query = input('enter query or back[b]: ')

        if query == 'b': return
        weights = input('inter weights(order: title, introduction, contents):')
        cluster_id = -1
        weights = [int(w) for w in weights.split()[:3]]
        filter_cluster = input('filter clusster[y|n]')
        if filter_cluster == 'y':
            cluster()
            cluster_id = input('enter cluseter_id')
        pageRank = (input('use pagerank [y|n]:') == 'y')
        e.search(weights, cluster_id, pageRank, query)
        while True:
            id = input('enter pk or back[b]:')
            if id == 'b': break
            e.get_doc(int(id))
        # if command == 'a':
        #     g.evaluation_all(measure, scoring_method)
        # else:
        #     g.evaluation(int(command), measure, scoring_method)

def cluster():
    title_to_id = e.get_clussters()
    for tittle, id in title_to_id.items():
        print('title:', tittle, '##########', id)

# print(list(range(1,5)))
main()