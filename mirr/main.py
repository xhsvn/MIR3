import json
import os

from WIKISEARCH.WikiSearcher import Elastic

e=Elastic()
title_to_pk={}
def make_index(name):
    e.create_index(name)

def read_json(dir):
    path_to_json = dir
    json_files = [pos_json for pos_json in os.listdir(path_to_json) if pos_json.endswith('.json')]
    for js in json_files:
        with open(os.path.join(path_to_json, js)) as json_file:
            temp = json.load(json_file)
            # print(temp)
            title_to_pk[temp['title']] = temp['pk']
            # print (temp)
            e.import_to_elastic(data=temp)



# make_index('wiki')
# read_json('WIKISEARCH/jsons')
# e.rm_index()
#
# print(title_to_pk)
all_docs = e.search(pagerank=True, weights=[1, 1, 1], query='منطق', )
print(all_docs)
# print(e.pagerank(0.2))