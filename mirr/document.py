from elasticsearch_dsl import DocType, Text, Integer, Keyword, Float


class Document(DocType):
    pk = Integer()
    url=Text()
    title=Text()
    introduction=Text()
    contents=Text()
    infobox=Text()
    pagerank = Float()
    cluster_id = Integer()
    cluster_title = Text()
    links = Keyword()

    def save(self, data, *args, **kwargs):
        self.pk = data['pk']
        self.url = data['url']
        self.title = data['title']
        self.introduction = data['introduction']
        self.contents = data['contents']
        self.infobox = data['infobox']
        self.pagerank = 0.0
        self.cluster_id = -1
        self.cluster_title = ""
        self.links = data['links']
        super(Document, self).save(*args, **kwargs)